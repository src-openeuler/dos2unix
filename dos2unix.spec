Name: dos2unix
Version: 7.5.2
Release: 1
Summary: Text file format converters
License: BSD-2-Clause
URL: http://waterlan.home.xs4all.nl/dos2unix.html
Source0: http://waterlan.home.xs4all.nl/dos2unix/%{name}-%{version}.tar.gz

BuildRequires: gcc gettext perl-Test-Harness perl-Test-Simple
Provides: unix2dos = %{version}
Obsoletes: unix2dos < %{version}

%description
The dos2unix utility converts characters in the DOS extended character set to the
corresponding ISO standard characters.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%make_build

%install
%make_install
rm -rf $RPM_BUILD_ROOT%{_docdir}
%find_lang %{name} --with-man --all-name

%check
make check

%files -f %{name}.lang
%license COPYING.txt
%{_bindir}/dos2unix
%{_bindir}/mac2unix
%{_bindir}/unix2dos
%{_bindir}/unix2mac

%files help
%{_mandir}/man1/*.1.gz

%changelog
* Fri Jan 26 2024 yanglongkang <yanglongkang@h-partners.com> - 7.5.2-1
- Update package to version 7.5.2
- Dos2unix can print info about the line break type of the last line, or indicate there is none.
- Updated documentation about the ASCII conversion mode.
- Fixed problem of converting a symbolic link target that is on another file system.
- Updated Chinese and Serbian translations.

* Thu Jul 13 2023 chenzixuan <chenzixuan@kylinos.cn> - 7.5.0-1
- Update package to version 7.5.0

* Mon Nov 7 2022 Bin Hu <hubin73@huawei.com> - 7.4.3-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update version 7.4.3

* Tue Oct 25 2022 yanglongkang <yanglongkang@h-partners.com> - 7.4.2-5
- rebuild for next release

* Tue Aug 02 2022 lixiaofei <lixiaofei38@huawei.com> - 7.4.2-4
- add BuildRequires perl for test

* Wed Jun 29 2022 lixiaofei <lixiaofei38@huawei.com> - 7.4.2-3
- update license to BSD-2-Clause

* Mon Oct 25 2021 caodongxia <caodongxia@huawei.com> - 7.4.2-2
- Add buildRequire perl-Test-Harness
 
* Thu Jan 21 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 7.4.2-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update to 7.4.2

* Thu Jul 23 2020 zhangxingliang <zhangxingliang3@huawei.com> - 7.4.1-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update to 7.4.1

* Mon Feb 17 2020 chengquan<chengquan3@huawei.com> - 7.4.0-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Enable test cases

* Fri Nov 22 2019 chengquan<chengquan3@huawei.com> - 7.4.0-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add COPYING file in main package

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 7.4.0-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.4.0-8
- Package init

